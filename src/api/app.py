from flask import Flask,request
from os import system

app = Flask(__name__)

def create_manifest(name,container_port,version):

    # Create values.yaml file to connect to the deployment and service yamls .
    values_yaml = f"""cat << EOF > {name}-values.yaml
name: {name}
container_port: {container_port}
image: 
    name: babaknasrolahy/{name}
    tag: {version}
EOF
                    """
    system(values_yaml)
    return "file was created"


@app.route('/create' , methods=['GET','POST'])
def create():
    data = request.get_json()
    name = data['name']
    container_port = data['container_port']
    platform = data['platfrom']
    envs = data['envs']
    version = data['version']
    
    # Create a directory with project's name 
    system(f"mkdir -p /codes/{name}/{version}/code")

    # Unzip code and Copy cods to its own directory
    system(f"unzip /zip_codes/{name}.zip -d /codes/{name}/{version}/code")

    # Remove orginal zip file
    system(f"rm -rf /zip_codes/{name}.zip")
    
    # Copy Dockerfile to the directory
    system(f"cp {platform}-Dockerfile /codes/{name}/{version}/code/Dockerfile")

    # Compelete the path with Create helm_chart directory
    system(f"mkdir -p /codes/{name}/{version}/helm_chart/templates/")

    # Create values.yalm file
    create_manifest(name,container_port,version)

    # Copy contents into the helm_char directory
    values = f"{name}-values.yaml"
    chart = "Chart.yaml"
    system(f"mv {values,chart}"+f" /codes/{name}/{version}/helm_chart/")
    system("cp {deployment.yaml,service.yaml}"+f" /codes/{name}/{version}/helm_chart/templates/")


    # Run docker command to Build Image
    system(f"docker --host tcp://image-builder:2375 build -t babaknasrolahy/{name}:v1 /codes/{name}/{version}/code/")

    # Push Docker Image into the repository
    system("docker --host tcp://image-builder:2375 login --username=babaknasrolahy --password=B@1383.Babak")
    system(f"docker --host tcp://image-builder:2375 push babaknasrolahy/{name}:{version}")



    # Helm command and deploy app
    system(f"helm --kubeconfig=k3s.yaml install {name} /codes/{name}/{version}/helm_chart/")


    return "this is create api"




@app.route('/zip_file/', methods=['POST'])
def file_create():
    file = request.files['file']
    file_name = file.filename
    file.save(file_name)  ## need to save on the /zip_codes path

    return 'File uploaded successfully!'



@app.route('/update' , methods=['GET','POST'])
def update():
    data = request.get_json()
    name = data['name']
    container_port = data['container_port']
    platform = data['platfrom']
    envs = data['envs']
    version = data['version']
    
    # Create a directory with project's name 
    system(f"mkdir -p /codes/{name}/{version}/code")

    # Unzip code and Copy cods to its own directory
    system(f"unzip {name}.zip -d /codes/{name}/{version}/code")

    # Remove orginal zip file
    system("rm -rf /zip_codes/{name}.zip")
    
    # Copy Dockerfile to the directory
    system(f"cp {platform}-Dockerfile /codes/{name}/{version}/code/Dockerfile")

    # Compelete the path with Create helm_chart directory
    system(f"mkdir -p /codes/{name}/{version}/helm_chart/templates/")

    # Create values.yalm file
    create_manifest(name,container_port,version)

    # Copy contents into the helm_char directory
    values = f"{name}-values.yaml"
    chart = "Chart.yaml"
    system(f"mv {values,chart}"+f" /codes/{name}/{version}/helm_chart/")
    system("cp {deployment.yaml,service.yaml}"+f" /codes/{name}/{version}/helm_chart/templates/")


    # Run docker command to Build Image
    system(f"docker --host tcp://image-builder:2375 build -t babaknasrolahy/{name}:v1 /codes/{name}/{version}/code/")

    # Push Docker Image into the repository
    system("docker --host tcp://image-builder:2375 login --username=babaknasrolahy --password=B@1383.Babak")
    system(f"docker --host tcp://image-builder:2375 push babaknasrolahy/{name}:{version}")


    # Helm command and deploy app
    system(f"helm --kubeconfig=k3s.yaml upgrade {name} /codes/{name}/{version}/helm_chart/")


    return "update"

if __name__ == '__main__':
    app.run(debug=True , port=5000)


