import requests

file_path = 'README.md'
url = 'http://localhost:5000/file/create'

with open(file_path, 'rb') as file:
    response = requests.post(url, files={'file': file})

print(response.text)

input()