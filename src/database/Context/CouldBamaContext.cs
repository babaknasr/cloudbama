﻿using CouldBama.DataBase.Common;
using CouldBama.DataBase.Entities.Apps;
using CouldBama.DataBase.Entities.Users;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace CouldBama.DataBase.Context
{
    public class CouldBamaContext : DbContext , ICouldBamaContext
    {
        public CouldBamaContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<UserInRole> UserInRoles { get; set; }
        public DbSet<Application> Applications { get; set; }
        public DbSet<Platform> Platforms { get; set; }
        public DbSet<PullRequest> PullRequests { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Role>().HasData(new {Id = (int)Common.Roles.Admin , Name = nameof(Common.Roles.Admin)});
            modelBuilder.Entity<Role>().HasData(new { Id = (int)Common.Roles.Customer, Name = nameof(Common.Roles.Customer) });

            modelBuilder.Entity<User>().HasMany(p => p.PullRequests).WithOne(u => u.User).HasForeignKey(p => p.UserId).OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<User>().HasIndex(c=>c.Email).IsUnique();

            modelBuilder.Entity<User>().HasQueryFilter(c => !c.IsDeleted); 
        }
    }
}
