﻿using CouldBama.DataBase.Entities.Apps;
using CouldBama.DataBase.Entities.Users;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace CouldBama.DataBase.Context
{
    public interface ICouldBamaContext
    {
        DbSet<User> Users { get; set; }
        DbSet<Role> Roles { get; set; }
        DbSet<UserInRole> UserInRoles { get; set; }
        DbSet<Application> Applications { get; set; }
        DbSet<Platform> Platforms { get; set; }
        DbSet<PullRequest> PullRequests { get; set; }


        public void Dispose();
        public int SaveChanges();
        public Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);
        public EntityEntry<TEntity> Entry<TEntity>(TEntity entity) where TEntity : class;
    }
}
