﻿using CouldBama.DataBase.Entities.Apps;
using CouldBama.DataBase.Services.Application;
using CouldBama.DataBase.Services.PullRequest;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace CouldBama.DataBase.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ApplicationController : ControllerBase
    {
        private readonly ILogger<ApplicationController> _logger;
        private readonly IApplicationService _application;

        public ApplicationController(ILogger<ApplicationController> logger, IApplicationService app)
        {
            _logger = logger;
            _application = app;
        }

        [HttpPost("Add",Name ="AddApplication")]
        public IActionResult Add([FromQuery] ApplicationDTO application)
        {
            string json = JsonConvert.SerializeObject(_application.Add(application));

            return Content(json, "application/json");
        }

        [HttpDelete("Delete", Name = "DeleteApplication")]
        public IActionResult Delete(Guid appId)
        {
            string json = JsonConvert.SerializeObject(_application.Delete(appId));

            return Content(json, "application/json");
        }

        [HttpGet("GetAll", Name = "GetAllApplications")]
        public IActionResult GetAll()
        {
            string json = JsonConvert.SerializeObject(_application.GetAll());

            return Content(json, "application/json");
        }

        [HttpGet("GetById", Name = "GetApplicationById")]
        public IActionResult GetById(Guid appId)
        {
            string json = JsonConvert.SerializeObject(_application.GetById(appId));

            return Content(json, "application/json");
        }

        [HttpGet("GetByUser", Name = "GetByUserApplication")]
        public IActionResult GetByUser(int userId)
        {
            string json = JsonConvert.SerializeObject(_application.GetByUser(userId));

            return Content(json, "application/json");
        }

        [HttpGet("UpdateVersion", Name = "UpdateApplicationVersion")]
        public IActionResult UpdateVersion(Guid appId)
        {
            string json = JsonConvert.SerializeObject(_application.UpdateVersion(appId));

            return Content(json, "application/json");
        }
    }
}
