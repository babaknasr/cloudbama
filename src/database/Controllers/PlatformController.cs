﻿using CouldBama.DataBase.Services.Platform;
using CouldBama.DataBase.Services.User;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace CouldBama.DataBase.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PlatformController : ControllerBase
    {
        private readonly ILogger<PlatformController> _logger;
        private readonly IPlatformService _platformService;

        public PlatformController(ILogger<PlatformController> logger, IPlatformService platform)
        {
            _logger = logger;
            _platformService = platform;
        }

        [HttpPost("Add", Name = "AddPlatform")]
        public IActionResult Add(string name,string imgPath)
        {
            string json = JsonConvert.SerializeObject(_platformService.Add(new PlatformDTO()
            {
                 ImgPath = imgPath,
                 Name = name,
            }));

            return Content(json, "application/json");
        }     

        [HttpDelete("Delete", Name = "DeletePlatform")]
        public IActionResult Delete(int id)
        {
            string json = JsonConvert.SerializeObject(_platformService.Delete(id));

            return Content(json, "application/json");
        }

        [HttpGet("GetAll", Name = "GetAllPlatforms")]
        public IActionResult GetAll()
        {
            string json = JsonConvert.SerializeObject(_platformService.GetAll());

            return Content(json, "application/json");
        }
    }
}
