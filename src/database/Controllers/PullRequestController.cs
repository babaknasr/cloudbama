﻿using CouldBama.DataBase.Services.Platform;
using CouldBama.DataBase.Services.PullRequest;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace CouldBama.DataBase.Controllers
{

    [ApiController]
    [Route("[controller]")]
    public class PullRequestController : ControllerBase
    {
        private readonly ILogger<PullRequestController> _logger;
        private readonly IPullRequestService _pullRequser;

        public PullRequestController(ILogger<PullRequestController> logger, IPullRequestService pull)
        {
            _logger = logger;
            _pullRequser = pull;
        }
        [HttpGet("GetAll", Name = "GetAllPullRequests")]
        public IActionResult GetAll()
        {
            string json = JsonConvert.SerializeObject(_pullRequser.GetAll());

            return Content(json, "application/json");
        }

        [HttpGet("GetByUser", Name = "GetPullRequestsByUser")]
        public IActionResult GetByUser(int userId)
        {
            string json = JsonConvert.SerializeObject(_pullRequser.GetByUser(userId));

            return Content(json, "application/json");
        }

        [HttpGet("GetByApplication", Name = "GetPullRequsetsByApplication")]
        public IActionResult GetByApplication(Guid applicationId)
        {
            string json = JsonConvert.SerializeObject(_pullRequser.GetByApplication(applicationId));

            return Content(json, "application/json");
        }

        [HttpGet("GetByDeploy", Name = "GetPullRequsestByDeploy")]
        public IActionResult GetByDeploy(bool isDeployed)
        {
            string json = JsonConvert.SerializeObject(_pullRequser.GetByDeploy(isDeployed));

            return Content(json, "application/json");
        }

        [HttpPost("Add",Name = "AddPullRequset")]
        public IActionResult Add([FromQuery] PullRequestDTO pullRequest)
        {
            string json = JsonConvert.SerializeObject(_pullRequser.Add(pullRequest));

            return Content(json, "application/json");
        }

        [HttpPost("IsFirst", Name = "IsFirstPullRequest")]
        public IActionResult IsFirst(Guid appId)
        {
            string json = JsonConvert.SerializeObject(_pullRequser.IsFirst(appId));

            return Content(json, "application/json");
        }
    }
}
