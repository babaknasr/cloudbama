﻿using CouldBama.DataBase.Services.User;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace CouldBama.DataBase.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UserController : Controller
    {
        private readonly ILogger<UserController> _logger;
        private readonly IUserService _userService;

        public UserController(ILogger<UserController> logger,IUserService user)
        {
            _logger = logger;
            _userService = user;
        }

        [HttpPost("Add",Name = "AddUser")]
        public IActionResult Add([FromQuery] UserDTO user)
        {
            string json = JsonConvert.SerializeObject(_userService.Add(user));

            return Content(json, "application/json");
        }

        [HttpPut("Update", Name = "UpdateUser")]
        public IActionResult Update(int id,string name , string password)
        {
            string json = JsonConvert.SerializeObject(_userService.Update(new UserDTO()
            {
                Id = id,
                Name = name,
                Password = password
            }));

            return Content(json, "application/json");
        }

        [HttpDelete("Delete", Name = "DeleteUser")]
        public IActionResult Delete(int id)
        {
            string json = JsonConvert.SerializeObject(_userService.Delete(id));

            return Content(json, "application/json");
        }

        [HttpGet("GetAll", Name = "GetAllUsers")]
        public IActionResult GetAll()
        {
            string json = JsonConvert.SerializeObject(_userService.GetAll());

            return Content(json, "application/json");
        }

        [HttpPost("LogIn", Name = "LoginUser")]
        public IActionResult Login(string email,string password)
        {
            string json = JsonConvert.SerializeObject(_userService.LoggedIn(email,password));

            return Content(json, "application/json");
        }

        [HttpGet("GetById", Name = "GetUserById")]
        public IActionResult GetById(int id)
        {
            string json = JsonConvert.SerializeObject(_userService.GetUserById(id));

            return Content(json, "application/json");
        }
    }
}
