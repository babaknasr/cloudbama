﻿using CouldBama.DataBase.Entities.Base;
using CouldBama.DataBase.Entities.Users;

namespace CouldBama.DataBase.Entities.Apps
{
    public class Application : BaseEntity<Guid>
    {
        public string Name { get; set; } = default!;
        public int Version { get; set; } = default!;

        public int Port { get; set; } = 80;

        public virtual Platform Platform { get; set; }
        public int PlatformId { get; set; }

        public virtual User User { get; set; }
        public int UserId { get; set; }

        public virtual ICollection<PullRequest> PullRequests { get; set; }
    }
}
