﻿using CouldBama.DataBase.Entities.Base;

namespace CouldBama.DataBase.Entities.Apps
{
    public class Platform:BaseEntity
    {
        public string Name { get; set; } = default!;
        public string ImagePath { get; set; } = default!;

        public virtual ICollection<Application> Applications { get; set; }

    }
}
