﻿using CouldBama.DataBase.Entities.Base;
using CouldBama.DataBase.Entities.Users;

namespace CouldBama.DataBase.Entities.Apps
{
    public class PullRequest : BaseEntity
    {
        public string FilePath { get; set; } = default!;
        public bool IsDeployed { get; set; }

        public virtual User User { get; set; }
        public int UserId { get; set; }

        public virtual Application Application { get; set; }
        public Guid ApplicationId { get; set; }
    }
}
