﻿namespace CouldBama.DataBase.Entities.Base
{
    public class BaseEntity<T>
    {
        public T Id { get; set; } = default!;
        public DateTime CreatedTime { get; set; } = DateTime.Now;
        public bool IsDeleted { get; set; } = false;
        public DateTime? DeletedTime { get; set; }

    }
    public class BaseEntity : BaseEntity<int>
    {

    }
}
