﻿using CouldBama.DataBase.Entities.Base;

namespace CouldBama.DataBase.Entities.Users
{
    public class Role
    {
        public int Id { get; set; }
        public string Name { get; set; } = default!;

        public virtual ICollection<UserInRole> Users { get; set; }
    }
}
