﻿using CouldBama.DataBase.Entities.Apps;
using CouldBama.DataBase.Entities.Base;

namespace CouldBama.DataBase.Entities.Users
{
    public class User : BaseEntity
    {
        public string Name { get; set; } = default!;
        public string Email { get; set; } = default!;
        public string Password { get; set; } = default!;

        public virtual ICollection<UserInRole> Roles { get; set; }
        public virtual ICollection<Application> Applications { get; set; }
        public virtual ICollection<PullRequest> PullRequests { get; set; }
    }
}
