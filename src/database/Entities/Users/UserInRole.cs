﻿using CouldBama.DataBase.Entities.Base;

namespace CouldBama.DataBase.Entities.Users
{
    public class UserInRole : BaseEntity
    {
        public virtual User User { get; set; }
        public int UserId { get; set; }

        public virtual Role Role { get; set; }
        public int RoleId { get; set; }

    }
}
