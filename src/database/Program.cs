using CouldBama.DataBase.Context;
using CouldBama.DataBase.Services.Application;
using CouldBama.DataBase.Services.Platform;
using CouldBama.DataBase.Services.PullRequest;
using CouldBama.DataBase.Services.User;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
string ConnectionString = builder.Configuration.GetConnectionString("CouldBamaDB");
builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddEntityFrameworkSqlServer().AddDbContext<CouldBamaContext>(options => options.UseSqlServer(ConnectionString));
builder.Services.AddScoped<ICouldBamaContext, CouldBamaContext>();
builder.Services.AddScoped<IUserService, UserService>();
builder.Services.AddScoped<IPlatformService, PlatformService>();
builder.Services.AddScoped<IPullRequestService, PullRequestService>();
builder.Services.AddScoped<IApplicationService, ApplicationService>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
