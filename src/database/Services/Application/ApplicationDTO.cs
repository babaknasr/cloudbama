﻿namespace CouldBama.DataBase.Services.Application
{
    public class ApplicationDTO
    {
        public Guid Id { get; set; }

        public string Name { get; set; }
        public int Version { get; set; }

        public int Port { get; set; } = 80;

        public bool IsDeployed { get; set; }

        public int PlatformId { get; set; }
        public int UserId { get; set; }

        public DateTime Created { get; set; }
    }
}
