﻿using CouldBama.DataBase.Common;
using CouldBama.DataBase.Context;
using CouldBama.DataBase.Entities.Apps;
using Microsoft.EntityFrameworkCore;

namespace CouldBama.DataBase.Services.Application
{
    public class ApplicationService : IApplicationService
    {
        private readonly ICouldBamaContext _db;
        public ApplicationService(ICouldBamaContext db)
        {
            _db = db;
        }
        public ResultDTO Add(ApplicationDTO applicationDTO)
        {
            var user = _db.Users.Find(applicationDTO.UserId);
            var platform = _db.Platforms.Find(applicationDTO.PlatformId);
            if (user == null)
            {
                return new ResultDTO()
                {
                    IsSuccess = false,
                    Message = "User not found!"
                };
            }
            if (platform == null)
            {
                return new ResultDTO()
                {
                    IsSuccess = false,
                    Message = "Platform not found!"
                };
            }

            var app = new Entities.Apps.Application()
            {
                Id = Guid.NewGuid(),
                Name = applicationDTO.Name,
                Platform = platform,
                PlatformId = applicationDTO.PlatformId,
                User = user,
                UserId = applicationDTO.UserId,
                Version = applicationDTO.Version,
                Port = applicationDTO.Port,
            };
            _db.Applications.Add(app);
            _db.SaveChanges();

            return new ResultDTO()
            {
                IsSuccess = true,
                Message = "Done!"
            };
        }

        public ResultDTO Delete(Guid applicationId)
        {
            var app = _db.Applications.Find(applicationId);

            if (app == null)
                return new ResultDTO()
                {
                    IsSuccess = false,
                    Message = "app Not found!"
                };
            _db.Applications.Remove(app);
            _db.SaveChanges();
            return new ResultDTO()
            {
                IsSuccess = true,
                Message = "Done!"
            };
        }

        public ResultDTO<List<ApplicationDTO>> GetAll()
        {
            return new ResultDTO<List<ApplicationDTO>>()
            {
                IsSuccess = true,
                Message = "Done!",
                Data = _db.Applications
                .Include(c => c.User)
                .Include(c => c.Platform)
                .Include(c => c.PullRequests)
                .Select(c => new ApplicationDTO()
                {
                    Id = c.Id,
                    Name = c.Name,
                    PlatformId = c.PlatformId,
                    UserId = c.UserId,
                   // IsDeployed =  GetIsDeployed(c.PullRequests.OrderByDescending(pr => pr.Id).FirstOrDefault()),
                    Version = c.Version,
                    Created = c.CreatedTime,
                    Port = c.Port,
                }).ToList()
            };
        }

        public ResultDTO<ApplicationDTO> GetById(Guid appId)
        {
            var application = _db.Applications.Find(appId);
            return new ResultDTO<ApplicationDTO>()
            {
                IsSuccess = true,
                Message = "done!",
                Data = new ApplicationDTO()
                {
                    Id = appId,
                    Created = application.CreatedTime,
                    Name = application.Name,
                    PlatformId = application.PlatformId,
                    UserId = application.UserId,
                   // IsDeployed = GetIsDeployed(application.PullRequests.OrderByDescending(pr => pr.Id).FirstOrDefault()),
                    Version = application.Version,
                    Port = application.Port,
                }
            };
        }

        public ResultDTO<List<ApplicationDTO>> GetByUser(int userId)
        {
            return new ResultDTO<List<ApplicationDTO>>()
            {
                IsSuccess = true,
                Message = "Done!",
                Data = _db.Applications
                .Include(c => c.User)
                .Include(c => c.Platform)
                .Where(c => c.UserId == userId)
                .Select(c => new ApplicationDTO()
                {
                    Id = c.Id,
                    Name = c.Name,
                    PlatformId = c.PlatformId,
                    UserId = userId,
                   // IsDeployed = GetIsDeployed(c.PullRequests.OrderByDescending(pr => pr.Id).FirstOrDefault()),
                    Version = c.Version,
                    Created = c.CreatedTime,
                    Port = c.Port,
                }).ToList()
            };
        }

        public ResultDTO<int> UpdateVersion(Guid appId)
        {
            var app = _db.Applications.Find(appId);
            if (app == null)
                return new ResultDTO<int>()
                {
                    IsSuccess = false,
                    Message = "app not found!"
                };

            app.Version++;
            _db.SaveChanges();

            return new ResultDTO<int>
            {
                IsSuccess = true,
                Data = app.Version,
                Message = "done!"
            };
        }

        private bool GetIsDeployed(Entities.Apps.PullRequest? pullRequest)
        {
            if (pullRequest == null) return false;
            return pullRequest.IsDeployed;
        }
    }
}
