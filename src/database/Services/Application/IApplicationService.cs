﻿using CouldBama.DataBase.Common;

namespace CouldBama.DataBase.Services.Application
{
    public interface IApplicationService
    {
        ResultDTO<List<ApplicationDTO>> GetByUser(int userId);
        ResultDTO<ApplicationDTO> GetById(Guid appId);
        ResultDTO<List<ApplicationDTO>> GetAll();
        ResultDTO Add(ApplicationDTO applicationDTO);
        ResultDTO Delete(Guid applicationId);
        ResultDTO<int> UpdateVersion(Guid appId);
    }
}
