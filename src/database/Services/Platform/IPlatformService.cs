﻿using CouldBama.DataBase.Common;
using CouldBama.DataBase.Entities.Apps;

namespace CouldBama.DataBase.Services.Platform
{
    public interface IPlatformService
    {
        ResultDTO Add(PlatformDTO platform);
        ResultDTO Delete(int id);
        ResultDTO<List<PlatformDTO>> GetAll();
    }
}
