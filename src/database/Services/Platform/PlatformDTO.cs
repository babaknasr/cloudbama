﻿namespace CouldBama.DataBase.Services.Platform
{
    public class PlatformDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ImgPath { get; set; }
        public Guid[] Applications { get; set; }
    }
}
