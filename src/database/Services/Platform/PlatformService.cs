﻿using CouldBama.DataBase.Common;
using CouldBama.DataBase.Context;
using Microsoft.EntityFrameworkCore;

namespace CouldBama.DataBase.Services.Platform
{
    public class PlatformService : IPlatformService
    {
        private readonly ICouldBamaContext _db;
        public PlatformService(ICouldBamaContext db)
        {
            _db = db;
        }
        public ResultDTO Add(PlatformDTO platform)
        {
            var plat = new Entities.Apps.Platform()
            {
                Name = platform.Name,
                ImagePath = platform.ImgPath,
            };
            _db.Platforms.Add(plat);
            _db.SaveChanges();
            return new ResultDTO()
            {
                IsSuccess = true,
                Message = "Done!"
            };
        }

        public ResultDTO Delete(int id)
        {
            var platform = _db.Platforms.Find(id);
            if (platform == null)
            {
                return new ResultDTO()
                {
                    IsSuccess = false,
                    Message = "Platform Not Found!!"
                };
            }

            _db.Platforms.Remove(platform);
            _db.SaveChanges();
            return new ResultDTO()
            {
                IsSuccess = true,
                Message = "Done!",
            };
        }

        public ResultDTO<List<PlatformDTO>> GetAll()
        {
            return new ResultDTO<List<PlatformDTO>>()
            {
                IsSuccess = true,
                Message = "Done!",
                Data = _db.Platforms
                .Include(c=>c.Applications)
                .Select(c => new PlatformDTO()
                {
                    Id = c.Id,
                    Name = c.Name,
                    ImgPath = c.ImagePath,
                    Applications = c.Applications.Select(a=>a.Id).ToArray()
                }).ToList()
            }
            ;
        }
    }
}
