﻿using CouldBama.DataBase.Common;

namespace CouldBama.DataBase.Services.PullRequest
{
    public interface IPullRequestService
    {
        ResultDTO<List<PullRequestDTO>> GetByApplication(Guid applicationId);
        ResultDTO<List<PullRequestDTO>> GetByUser(int userId);
        ResultDTO<List<PullRequestDTO>> GetByDeploy(bool isDeployed);
        ResultDTO<List<PullRequestDTO>> GetAll();
        ResultDTO<bool> IsFirst(Guid applicationId);
        ResultDTO Add(PullRequestDTO request);
    }
}
