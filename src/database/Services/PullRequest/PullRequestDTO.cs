﻿namespace CouldBama.DataBase.Services.PullRequest
{
    public class PullRequestDTO
    {
        public int Id { get; set; }
        public string FilePath { get; set; }
        public DateTime CreatedTime { get; set; }
        public bool IsDeployed { get; set; }
        public int UserId { get; set; }
        public Guid ApplicationId { get; set; }
    }
}
