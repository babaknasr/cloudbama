﻿using CouldBama.DataBase.Common;
using CouldBama.DataBase.Context;
using Microsoft.EntityFrameworkCore;

namespace CouldBama.DataBase.Services.PullRequest
{
    public class PullRequestService : IPullRequestService
    {
        private readonly ICouldBamaContext _db;
        public PullRequestService(ICouldBamaContext db)
        {
            _db = db;
        }

        public ResultDTO Add(PullRequestDTO request)
        {
            var r = new Entities.Apps.PullRequest();
            var app = _db.Applications.Find(request.ApplicationId);
            var user = _db.Users.Find(request.UserId);
            if (app == null)
            {
                return new ResultDTO()
                {
                    IsSuccess = false,
                    Message = "Application Not found!!"
                };
            }
            if (user == null)
            {
                return new ResultDTO()
                {
                    IsSuccess = false,
                    Message = "User Not found!!"
                };
            }
            r.Application = app;
            r.ApplicationId = app.Id;
            r.User = user;
            r.UserId = user.Id;
            r.IsDeployed = request.IsDeployed;
            r.FilePath = request.FilePath;
            _db.PullRequests.Add(r);
            _db.SaveChanges();

            return new ResultDTO()
            {
                IsSuccess = true,
                Message = "Done!"
            };
        }

        public ResultDTO<List<PullRequestDTO>> GetAll()
        {
            return new ResultDTO<List<PullRequestDTO>>()
            {
                IsSuccess = true,
                Message = "Done!",
                Data = _db.PullRequests
                .Include(c => c.Application)
                .Include(c => c.User)
                .Select(c => new PullRequestDTO()
                {
                    Id = c.Id,
                    ApplicationId = c.ApplicationId,
                    UserId = c.UserId,
                    CreatedTime = c.CreatedTime,
                    IsDeployed = c.IsDeployed,
                    FilePath = c.FilePath,
                }).ToList()
            };
        }

        public ResultDTO<List<PullRequestDTO>> GetByApplication(Guid applicationId)
        {
            return new ResultDTO<List<PullRequestDTO>>()
            {
                IsSuccess = true,
                Message = "Done!",
                Data = _db.PullRequests
                .Include(c => c.Application)
                .Include(c => c.User)
                .Where(c=>c.ApplicationId == applicationId)
                .Select(c => new PullRequestDTO()
                {
                    Id = c.Id,
                    ApplicationId = c.ApplicationId,
                    UserId = c.UserId,
                    CreatedTime = c.CreatedTime,
                    IsDeployed = c.IsDeployed,
                    FilePath= c.FilePath,
                }).ToList()
            };
        }

        public ResultDTO<List<PullRequestDTO>> GetByDeploy(bool isDeployed)
        {
            return new ResultDTO<List<PullRequestDTO>>()
            {
                IsSuccess = true,
                Message = "Done!",
                Data = _db.PullRequests
                .Include(c => c.Application)
                .Include(c => c.User)
                .Where(c=>c.IsDeployed == isDeployed)
                .Select(c => new PullRequestDTO()
                {
                    Id = c.Id,
                    ApplicationId = c.ApplicationId,
                    UserId = c.UserId,
                    CreatedTime = c.CreatedTime,
                    IsDeployed = c.IsDeployed,
                    FilePath= c.FilePath,
                }).ToList()
            };
        }

        public ResultDTO<List<PullRequestDTO>> GetByUser(int userId)
        {
            return new ResultDTO<List<PullRequestDTO>>()
            {
                IsSuccess = true,
                Message = "Done!",
                Data = _db.PullRequests
                .Include(c => c.Application)
                .Include(c => c.User)
                .Where(c=>c.User.Id == userId)
                .Select(c => new PullRequestDTO()
                {
                    Id = c.Id,
                    ApplicationId = c.ApplicationId,
                    UserId = c.UserId,
                    CreatedTime = c.CreatedTime,
                    IsDeployed = c.IsDeployed,
                    FilePath= c.FilePath,
                }).ToList()
            };
        }

        public ResultDTO<bool> IsFirst(Guid applicationId)
        {
            return new ResultDTO<bool>
            {
                IsSuccess = true,
                Message = "Done!",
                Data = _db.PullRequests
            .Include(c => c.Application)
            .Any(c => c.ApplicationId == applicationId)
            };
        }
    }
}
