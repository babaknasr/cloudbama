﻿using CouldBama.DataBase.Common;

namespace CouldBama.DataBase.Services.User
{
    public interface IUserService
    {
        ResultDTO<IList<UserDTO>> GetAll();
        ResultDTO<UserDTO> GetUserById(int id);
        ResultDTO<UserDTO> LoggedIn(string email,string password);

        ResultDTO<int> Add(UserDTO user);
        ResultDTO Update(UserDTO user);
        ResultDTO Delete(int id);
    }
}
