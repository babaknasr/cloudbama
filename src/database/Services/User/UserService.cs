﻿using CouldBama.DataBase.Common;
using CouldBama.DataBase.Context;
using CouldBama.DataBase.Entities.Users;
using Microsoft.EntityFrameworkCore;

namespace CouldBama.DataBase.Services.User
{
    public class UserService : IUserService
    {
        private readonly ICouldBamaContext _db;
        public UserService(ICouldBamaContext db)
        {
            _db = db;
        }
        public ResultDTO<int> Add(UserDTO user)
        {
            try
            {
                var User = new Entities.Users.User()
                {
                    Email = user.Email,
                    Name = user.Name,
                    Password = user.Password,
                };
                var roles = new List<UserInRole>();
                foreach (var item in user.Roles.Split(','))
                {
                    var role = _db.Roles.SingleOrDefault(c => c.Name.ToLower() == item.ToLower());
                    var userInRole = new UserInRole()
                    {
                        Role = role,
                        RoleId = role.Id,
                        User = User,
                        UserId = User.Id

                    };
                    roles.Add(userInRole);
                }
                User.Roles = roles;
                _db.Users.Add(User);
                _db.SaveChanges();

                return new ResultDTO<int>()
                {
                    IsSuccess = true,
                    Message = "Done!",
                    Data = User.Id,
                };
            }
            catch (DbUpdateException)
            {
                return new ResultDTO<int>()
                {
                    IsSuccess = false,
                    Message = "email is already exist!",
                };
            }

        }

        public ResultDTO Delete(int id)
        {
            var user = _db.Users.Find(id);
            user.IsDeleted = true;
            _db.SaveChanges();
            return new ResultDTO()
            {
                IsSuccess = true,
                Message = "Done!"
            };
        }

        public ResultDTO<IList<UserDTO>> GetAll()
        {
            var users = _db.Users
               .Include(c => c.Roles)
               .Select(c => new UserDTO()
               {
                   Id = c.Id,
                   Email = c.Email,
                   Name = c.Name,
                   Password = c.Password,
                   Roles = string.Join(",", c.Roles.Select(s => s.Role.Name)),
               }).ToList();
            return new ResultDTO<IList<UserDTO>>()
            {
                IsSuccess = true,
                Message = "Done!",
                Data = users
            };
        }

        public ResultDTO<UserDTO> GetUserById(int id)
        {
            var c = _db.Users
                .Include(c => c.Roles)
                .ThenInclude(c => c.Role)
                .SingleOrDefault(c => c.Id == id);
            if (c == null)
                return new ResultDTO<UserDTO>()
                {
                    IsSuccess = false,
                    Message = "User Not Found!"
                };
            return new ResultDTO<UserDTO>()
            {
                IsSuccess = true,
                Message = "Done!",
                Data = new UserDTO()
                {
                    Id = c.Id,
                    Email = c.Email,
                    Name = c.Name,
                    Password = c.Password,
                    Roles = string.Join(",", c.Roles.Select(s => s.Role.Name)),
                }
            };
        }

        public ResultDTO<UserDTO> LoggedIn(string email, string password)
        {
            var user = _db.Users
                .Include(c=>c.Roles)
                .ThenInclude(c=>c.Role)
                .SingleOrDefault(u => u.Email == email);
            if (null == user)
            {
                return new ResultDTO<UserDTO>()
                {
                    IsSuccess = false,
                    Message = "User not found!!"
                };
            }
            if (user.Password != password)
            {
                return new ResultDTO<UserDTO>()
                {
                    IsSuccess = false,
                    Message = "Password is not true!!"
                };
            }
            return new ResultDTO<UserDTO>()
            {
                IsSuccess = true,
                Message = "done!",
                Data = new UserDTO()
                {
                    Id = user.Id,
                    Name = user.Name,
                    Password = user.Password,
                    Email = user.Email,
                    Roles = user.Roles.FirstOrDefault()?.Role.Name ?? ""
                }
            };
        }

        public ResultDTO Update(UserDTO user)
        {
            var User = _db.Users.Find(user.Id);
            User.Name = user.Name;
            User.Password = user.Password;
            _db.SaveChanges();

            return new ResultDTO()
            {
                IsSuccess = true,
                Message = "Done!"
            };
        }
    }
}
