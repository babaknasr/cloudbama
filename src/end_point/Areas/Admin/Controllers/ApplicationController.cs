﻿using CouldBama.EndPoint.Areas.Admin.Models;
using CouldBama.EndPoint.Common;
using CouldBama.EndPoint.Entities;
using CouldBama.EndPoint.utility;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace CouldBama.EndPoint.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class ApplicationController : Controller
    {
        private readonly Api _api;
        public ApplicationController(IHttpClientFactory factory)
        {
            _api = new Api(factory);
        } 
        public async Task<IActionResult> Index()
        {
            var res = await _api.CallApiGet("/application/getall");
            var data = JsonConvert.DeserializeObject<ResultDTO<List<ApplicationDTO>>>(res).Data;
            return View(data.Select(async c=>new ApplicationAdminViewModel()
            {
                Id = c.Id,
                Name = c.Name,
                Created = c.Created,
                IsDeploy = c.IsDeployed,
                Port = c.Port,
                Version = c.Version,
                UserId = c.UserId,
                Platform = await GetPlatformName(c.PlatformId)
            }));
        }

        [Route("admin/application/detail/{appId}")]
        public async Task<IActionResult> Detail(Guid appId)
        {
            var res = await _api.CallApiGet($"application/getById?appId={appId}");
            var app = JsonConvert.DeserializeObject<ResultDTO<ApplicationDTO>>(res).Data;

            res = await _api.CallApiGet($"pullrequest/getByapplication?appId={appId}");
            var pullRequests = JsonConvert.DeserializeObject<ResultDTO<List<PullRequestDTO>>>(res).Data;

            return View(new ApplicationAdminViewModel()
            {
                Id = appId,
                Created = app.Created,
                IsDeploy = app.IsDeployed,
                Port = app.Port,
                Version = app.Version,
                UserId = app.UserId,
                Name = app.Name,
                Platform = await GetPlatformName(app.PlatformId),
                PullRequests = pullRequests,
            });
        }

        [NonAction]
        private async Task<string> GetPlatformName(int id)
        {
            var res = await _api.CallApiGet("paltform/getall");
            var data = JsonConvert.DeserializeObject<ResultDTO<List<PlatformDTO>>>(res).Data;
            return data.First(c=>c.Id == id).Name;
        }
    }
}
