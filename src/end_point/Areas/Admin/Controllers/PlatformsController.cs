﻿using CouldBama.EndPoint.Common;
using CouldBama.EndPoint.Entities;
using CouldBama.EndPoint.utility;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;
using System.Text.Json.Serialization;

namespace CouldBama.EndPoint.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class PlatformsController : Controller
    {
        private readonly Api api;
        public PlatformsController(IHttpClientFactory httpClientFactory)
        {
            api = new Api(httpClientFactory);
        }
        public async Task<IActionResult> Index()
        {
            var res = await api.CallApiGet("/platform/GetAll");
            var users = JsonConvert.DeserializeObject<ResultDTO<List<PlatformDTO>>>(res).Data;
            return View(users);
        }
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(string name,string imgPath)
        {
            var res = await api.CallApiPost($"/platform/add?name={name}&imgPath={imgPath}");
            return Redirect("/admin/platforms");
        }
        [HttpPost]
        public async Task<IActionResult> Delete(int platformId)
        {
            var res = await api.CallApiDelete($"/platform/delete?id={platformId}");
            return Json(res);
        }
    }
}
