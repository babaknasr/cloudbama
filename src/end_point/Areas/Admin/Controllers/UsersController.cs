﻿using CouldBama.EndPoint.Common;
using CouldBama.EndPoint.Entities;
using CouldBama.EndPoint.utility;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;
using System.Text.Json.Serialization;

namespace CouldBama.EndPoint.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class UsersController : Controller
    {
        private readonly Api api;
        public UsersController(IHttpClientFactory httpClientFactory)
        {
            api = new Api(httpClientFactory);
        }
        public async Task<IActionResult> Index()
        {
            var res = await api.CallApiGet("/user/GetAll");
            var users = JsonConvert.DeserializeObject<ResultDTO<List<UserDTO>>>(res)?.Data ?? new List<UserDTO>();
            return View(users);
        }
        public IActionResult Create()
        {
            ViewBag.Roles = new SelectList(Enum.GetNames(typeof(Roles)));
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(string name,string email,string password,string role)
        {
            var res = await api.CallApiPost($"/user/add?name={name}&email={email}&password={password}&roles={role}");
            return RedirectToAction(nameof(Index));
        }
        [HttpPost]
        public async Task<IActionResult> Delete(int userId)
        {
            var res = await api.CallApiDelete($"/user/delete?id={userId}");
            return Json(res);
        }
    }
}
