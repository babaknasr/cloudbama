﻿using CouldBama.EndPoint.Entities;

namespace CouldBama.EndPoint.Areas.Admin.Models
{
    public class ApplicationAdminViewModel
    {

        public Guid Id { get; set; }

        public string Name { get; set; }
        public int UserId {  get; set; }
        public int Version { get; set; }

        public int Port { get; set; }

        public bool IsDeploy { get; set; }

        public string Platform { get; set; }

        public DateTime Created { get; set; }

        public List<PullRequestDTO> PullRequests { get; set; }
    }
}
