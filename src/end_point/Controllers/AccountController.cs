﻿using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using CouldBama.EndPoint.Entities;
using System.Security.Claims;
using System.Net.Http;
using System.Text;
using Newtonsoft.Json;
using CouldBama.EndPoint.Common;
using System.Xml.Linq;
using CouldBama.EndPoint.utility;

namespace CouldBama.EndPoint.Controllers
{
    public class AccountController : Controller
    {
        private readonly Api api;
        public AccountController(IHttpClientFactory httpClient)
        {
            api = new Api(httpClient);
        }

        public IActionResult SingUp()
        {
            return View();
        }

        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> SingUp(string name, string email, string password, bool rememberMe)
        {
            var signupResult = await api.CallApiPost($"/user/add?name={name}&email={email}&password={password}&roles=customer");

            var res = JsonConvert.DeserializeObject<ResultDTO<int>>(signupResult);
            if (res.IsSuccess)
            {
                var claims = new List<Claim>()
                {
                    new Claim(ClaimTypes.NameIdentifier,res.Data.ToString()),
                    new Claim(ClaimTypes.Email, email),
                    new Claim(ClaimTypes.Name, name),
                    new Claim(ClaimTypes.Role, "Customer"),
                };


                var identity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
                var principal = new ClaimsPrincipal(identity);
                var properties = new AuthenticationProperties()
                {
                    IsPersistent = rememberMe
                };
                await HttpContext.SignInAsync(principal, properties);

            }
            else
            {
                ViewBag.alert = res.Message;
            }
            return Redirect("/");
        }

        [HttpPost]
        public async Task<IActionResult> Login(string email, string password, bool rememberMe)
        {
            var signupResult = await api.CallApiPost($"/user/login?email={email}&password={password}");

            var res = JsonConvert.DeserializeObject<ResultDTO<UserDTO>>(signupResult);
            if (res.IsSuccess)
            {
                var claims = new List<Claim>()
                {
                    new Claim(ClaimTypes.NameIdentifier,res.Data.Id.ToString()),
                    new Claim(ClaimTypes.Email, email),
                    new Claim(ClaimTypes.Name, res.Data.Name),
                    new Claim(ClaimTypes.Role, res.Data.Roles),
                };

                var identity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
                var principal = new ClaimsPrincipal(identity);
                var properties = new AuthenticationProperties()
                {
                    IsPersistent = rememberMe,
                    ExpiresUtc = DateTime.Now.AddDays(100),
                };
                await HttpContext.SignInAsync(principal, properties);
                return Redirect("/");

            }
            else
            {
                ViewBag.alert = res.Message;
                return View(res);
            }
        }
    }
}
