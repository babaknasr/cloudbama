﻿using CouldBama.EndPoint.Common;
using CouldBama.EndPoint.Entities;
using CouldBama.EndPoint.Models;
using CouldBama.EndPoint.utility;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;

namespace CouldBama.EndPoint.Controllers
{
    [Authorize(Roles = "Customer, Admin")]
    public class ApplicationController : Controller
    {
        private readonly Api _api;
        private readonly Api _pythonApi;
        private readonly string wwwRootPath;
        public ApplicationController(IHttpClientFactory httpClient, IWebHostEnvironment environment)
        {
            _api = new Api(httpClientFactory: httpClient);
            _pythonApi = new Api(httpClientFactory: httpClient, "http://172.25.4.77:5000");
            wwwRootPath = environment.WebRootPath;
        }

        [HttpGet]
        [Route("/Panel/Application/Create")]
        public async Task<IActionResult> Create()
        {
            var res = await _api.CallApiGet("/platform/GetAll");
            var platforms = JsonConvert.DeserializeObject<ResultDTO<List<PlatformDTO>>>(res);
            ViewBag.Platforms = new SelectList(platforms.Data, "Id", "Name");
            return View();
        }

        [HttpPost]
        [Route("/panel/application/Create")]
        public async Task<IActionResult> Create(string name, int port, int platform)
        {
            var res = await _api.CallApiPost($"/application/add?name={name}&port={port}&PlatformId={platform}&UserId={ClaimUtility.GetUserId(User)}&Version=1");
            var convertedRes = JsonConvert.DeserializeObject<ResultDTO>(res);

            if (convertedRes.IsSuccess)
                return Redirect("/panel");

            ViewBag.Error = convertedRes.Message;
            return View();
        }

        [HttpPost]
        [Route("/panel/application/Delete")]
        public async Task<IActionResult> Delete(Guid appId)
        {
            var res = await _api.CallApiDelete($"/application/Delete?appId={appId}");
            var isSuccess = JsonConvert.DeserializeObject<ResultDTO>(res).IsSuccess;
            try
            {
                if (isSuccess)
                    Directory.Delete(wwwRootPath + "/" + appId, true);
            }catch (DirectoryNotFoundException){}
           
            return Ok(res);
        }

        [HttpGet]
        [Route("/panel/application/Detail/{appId}")]
        public async Task<IActionResult> Detail(Guid appId)
        {
            var res = await _api.CallApiGet($"/application/getById?appId={appId}");
            var app = JsonConvert.DeserializeObject<ResultDTO<ApplicationDTO>>(res)?.Data??null;
            if (app == null)
                return NotFound();

            res = await _api.CallApiGet("/platform/GetAll");
            var platforms = JsonConvert.DeserializeObject<ResultDTO<List<PlatformDTO>>>(res).Data;

            res = await _api.CallApiGet($"/pullrequest/GetByApplication?applicationId={appId}");
            var requests = JsonConvert.DeserializeObject<ResultDTO<List<PullRequestDTO>>>(res).Data;

            return View(new ApplicationViewModel()
            {
                Created = app.Created,
                Id = app.Id,
                Name = app.Name,
                Platform = platforms.Single(p => p.Id == app.PlatformId).Name,
                Port = app.Port,
                Version = app.Version,
                PullRequests = requests
            });
        }

        [Route("/panel/application/pullrequest/{appId}")]
        public IActionResult CreatePullRequest(Guid appId)
        {
            ViewBag.AppId = appId;
            return View();
        }

        [HttpPost]
        [Route("/Panel/application/pullrequest")]
        public async Task<IActionResult> CreatePullRequest(IFormFile file, Guid appId)
        {
            var res = await _api.CallApiPost($"/pullRequest/IsFirst?appId={appId}");
            var deserial = JsonConvert.DeserializeObject<ResultDTO<bool>>(res);
            if (!deserial.Data)
            {
                res = await _pythonApi.PostJsonToApi("/create", new
                {
                    name = "test",
                    platform = "flask",
                    container_port = 5000,
                    version = 1,
                });
                var isDeploy = JsonConvert.DeserializeObject<ResultDTO>(res ?? "")?.IsSuccess ?? true;
                res = await _api.CallApiPost
                    ($"/PullRequest/add?IsDeployed={isDeploy}&UserId={ClaimUtility.GetUserId(User)}&applicationId={appId}&filePath={UploadFile(file, appId, wwwRootPath)}");
            }
            else
            {
                res = await _api.CallApiGet($"/application/UpdateVersion?appId={appId}");
                int appVersion = JsonConvert.DeserializeObject<ResultDTO<int>>(res).Data;
                
                res = await _pythonApi.PostJsonToApi("/update", new
                {
                    name = "test",
                    platform = "flask",
                    container_port = 5000,
                    version = appVersion,
                });
                var isDeploy = JsonConvert.DeserializeObject<ResultDTO>(res ?? "")?.IsSuccess ?? true;
                res = await _api.CallApiPost
                    ($"/PullRequest/add?IsDeployed={isDeploy}&UserId={ClaimUtility.GetUserId(User)}&applicationId={appId}&filePath={UploadFile(file, appId, wwwRootPath)}");
            }
            return Redirect($"/panel/application/detail/{appId}");
        }

        [NonAction]
        private string UploadFile(IFormFile file, Guid appId, string root)
        {
            string path = @$"{root}\Files\{appId}\";
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            Guid name = Guid.NewGuid();
            string fullPath = path + name + ".zip";
            using (FileStream fs = new FileStream(fullPath, FileMode.Create))
            {
                file.CopyTo(fs);
            }
            return fullPath;
        }
    }
}
