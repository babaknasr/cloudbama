﻿using CouldBama.EndPoint.Common;
using CouldBama.EndPoint.Entities;
using CouldBama.EndPoint.Models;
using CouldBama.EndPoint.utility;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace CouldBama.EndPoint.Controllers
{
    [Authorize(Roles ="Customer, Admin")]
    public class PanelController : Controller
    {
        private readonly Api _api;
        public PanelController(IHttpClientFactory httpClient)
        {
            _api = new Api(httpClient);
        }
        public async Task<IActionResult> Index()
        {
            var res = await _api.CallApiGet($"/Application/GetByUser?userId={ClaimUtility.GetUserId(User)}");
            var apps = JsonConvert.DeserializeObject<ResultDTO<List<ApplicationDTO>>>(res).Data;

            res = await _api.CallApiGet("/platform/GetAll");
            var platforms = JsonConvert.DeserializeObject<ResultDTO<List<PlatformDTO>>>(res).Data; 
            return View(apps.Select(c=>new ApplicationViewModel()
            {
                Id = c.Id,
                Name = c.Name,
                Created = c.Created,
                Version = c.Version,
                Platform = platforms.Single(p=>p.Id == c.PlatformId).Name
            }).ToList());
        }

        public IActionResult PullRequests()
        {
            return View();
        }
    }
}
