﻿namespace CouldBama.EndPoint.Entities
{
    public class UserDTO
    {
        public int Id { get; set; }
        public string Name { get; set; } = default!;
        public string Email { get; set; } = default!;
        public string Password { get; set; } = default!;
        public string Roles { get; set; } = default!;

    }
}
