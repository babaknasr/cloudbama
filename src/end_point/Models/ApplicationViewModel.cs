﻿using CouldBama.EndPoint.Entities;

namespace CouldBama.EndPoint.Models
{
    public class ApplicationViewModel
    {

        public Guid Id { get; set; }

        public string Name { get; set; }
        public int Version { get; set; }

        public int Port { get; set; }

        public bool IsDeploy { get; set; }

        public string Platform { get; set; }

        public DateTime Created { get; set; }

        public  List<PullRequestDTO> PullRequests { get; set; }
    }
}
