﻿using Newtonsoft.Json;
using System.Text;

namespace CouldBama.EndPoint.utility
{
    public class Api
    {
        private readonly HttpClient _httpClient;

        public Api(IHttpClientFactory httpClientFactory , string uri = "https://localhost:5001/")
        {
            _httpClient = httpClientFactory.CreateClient();
            _httpClient.BaseAddress = new Uri(uri);
        }
        public async Task<string> CallApiPost(string api)
        {
            try
            {
                var response = await _httpClient.PostAsync(api, null);

                if (response.IsSuccessStatusCode)
                {
                    var responseData = await response.Content.ReadAsStringAsync();
                    // Process the successful response data
                    return responseData;
                }
                else
                {
                    var errorResponse = await response.Content.ReadAsStringAsync();
                    // Handle the error response
                    return errorResponse;
                }
            }
            catch
            {
                return "";
            }
        }
        public async Task<string> CallApiGet(string api)
        {
            try
            {
                var response = await _httpClient.GetAsync(api);

                if (response.IsSuccessStatusCode)
                {
                    var responseData = await response.Content.ReadAsStringAsync();
                    // Process the successful response data
                    return responseData;
                }
                else
                {
                    var errorResponse = await response.Content.ReadAsStringAsync();
                    // Handle the error response
                    return errorResponse;
                }
            }
            catch
            {
                return "";
            }
        }
        public async Task<string> CallApiDelete(string api)
        {
            try
            {
                var response = await _httpClient.DeleteAsync(api);

                if (response.IsSuccessStatusCode)
                {
                    var responseData = await response.Content.ReadAsStringAsync();
                    // Process the successful response data
                    return responseData;
                }
                else
                {
                    var errorResponse = await response.Content.ReadAsStringAsync();
                    // Handle the error response
                    return errorResponse;
                }
            }
            catch
            {
                return "";
            }
        }
        public async Task<string> CallApiPut(string api)
        {
            try
            {
                var response = await _httpClient.PutAsync(api, null);

                if (response.IsSuccessStatusCode)
                {
                    var responseData = await response.Content.ReadAsStringAsync();
                    // Process the successful response data
                    return responseData;
                }
                else
                {
                    var errorResponse = await response.Content.ReadAsStringAsync();
                    // Handle the error response
                    return errorResponse;
                }
            }
            catch
            {
                return "";
            }
        }

        public async Task<string> PostJsonToApi(string apiUrl, object jsonData)
        {
            try
            {
                var json = JsonConvert.SerializeObject(jsonData);
                var content = new StringContent(json, Encoding.UTF8, "application/json");

                var response = await _httpClient.PostAsync(apiUrl, content);

                response.EnsureSuccessStatusCode();

                var responseContent = await response.Content.ReadAsStringAsync();

                return responseContent;
            }
            catch (Exception ex)
            {
                // Handle any exceptions that occurred during the API call
                Console.WriteLine(ex.Message);
                return null;
            }
        }

    }

}
