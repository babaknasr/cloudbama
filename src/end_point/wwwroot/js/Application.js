﻿function Delete(id) {
    var postData = {
        'appId': id,
    };
    $.ajax({
        contentType: 'application/x-www-form-urlencoded',
        dataType: 'json',
        type: "POST",
        url: "/panel/application/Delete",
        data: postData,
        success: function (data) {
            if (data.IsSuccess == true) {
                window.location.href = "/panel";
            }
            else {
                alert("Error!");
            }
        },
        error: function (request, status, error) {
            alert(request.responseText);
        }
    });
}