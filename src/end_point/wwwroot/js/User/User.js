﻿function Delete(id) {
    var postData = {
        'userId': id,        
    };
    $.ajax({
        contentType: 'application/x-www-form-urlencoded',
        dataType: 'json',
        type: "POST",
        url: "Users/Delete",
        data: postData,
        success: function (data) {
            if (data.IsSuccess == true) {
                alert("Done!");
            }
            else {
                alert("Error!");
            }
        },
        error: function (request, status, error) {
            alert(request.responseText);
        }
    });
}